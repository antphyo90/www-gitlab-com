---
layout: handbook-page-toc
title: "Deal Desk"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to The Deal Desk Handbook 

### Charter

The purpose of Deal Desk is to streamline the opportunity management process while acting as a trusted business partner for field sales. We are the first point of contact for sales support.

## Key Focus Areas
    
*  Sales Support
*  Renewals
*  Month End / Quarter End Close

### Sales Support 

### Communication Overview

#### Sales Ops / Deal Desk SLA 

The Deal Desk team will do their best to respond to each request within 4 hours. Revenue generating, current quarter requests will take priority, especially during Month & Quarter End. If a task is not resolved within 24 hours it will be escalated (if necessary). 

| Type of Request | First Response | Resolution |
|----- | ----- | ------| 
| Basic Quote Assistance | 6 Hours | 8 Hours | 
| Ramp Deal | 6 Hours | 24 hours |
| Flat Renewal | 6 Hours | 24 Hours |
| IACV Calculation | 6 Hours | 24 Hours |
| Contract Reset / Co-Term | 6 Hours | 24 Hours | 
| RFP/Vendor Forms | 6 Hours | Dependent on AM |

##### Salesforce Chatter Communication Basics

Deal Desk's primary communication channel is Salesforce Chatter. When you chatter `@Sales-Support`, it will automatically create a case in the Deal Desk queue. Deal Desk team members monitor the queue daily and will respond to a case within 6 hours, Monday-Friday, with the exception of National/Regional holidays. Resolution or escalation will occur within 24 hours. 

To Chatter the DD team, tag `@Sales-Support`in the comment field on the related opportunity or account page and a short sentence on your request. If the Deal Desk team needs more information, we will follow up directly via Chatter.

Please avoid tagging Deal Desk team members directly in chatter, instead use @Sales-Support to ensure coverage in case the DD team member who replied first is OOO. If someone is working on a case, they will continue to support until the case is closed.  If an issue has been resolved, you need to chatter @Sales-Support to reopen a case.


##### Monitoring the Sales Ops Case Queue

The Deal Desk team is located around the world and will be available during standard business hours within most regions. 

**EMEA**
*  Meri Gil Galindo - Dublin, Ireland
*  Marcsi Szucs - Budapest, Hungary
    

 **AMER**
*  Jesse Rabbits - New York, NY 
*  Cal Baker - Seattle, WA
    

**APAC**
*  TBH 



##### Chatter Automatic Replies

SF Chatter will send automatic replies on behalf of the DD member who changed the status of the case (by tagging the requestor in the Chatter comment):
Status change to “In Progress” will generate “I am working on this” reply.
Status change to “Closed” will generate “This is all set” auto reply in Chatter.

In order to switch off the auto reply function, the checkbox “Suppress auto case reply?” should be ticked under each case.


### Slack

##### Main Slack Channel

Use our Slack channel in case of general, non-record related requests and/or urgent questions: 
**#sales-support**

If the request is related to a quote, opportunity, or acccount - please chatter on the page in Salesforce instead of the Slack channel. 

##### Slack Best Practices

Please avoid contacting the DD team members directly via Slack. Utlizing the channel is best to ensure timely coverage and helps others who may have simliar questions.

In cae of a specific opportunity or quote related question please use SF Chatter (see section [Salesforce Chatter Commnunication](##### Salesforce Chatter Communication Basics))

##### Announcements

Desk Desk process updates and announcements will be communicated via **#Sales** Slack channel. 

Monitoring #sales-support - Best Practices link to come!

##### Deal Desk Internal Slack Channels

**#deal-desk-ops** - Deal Desk, Billing Team channel for discussion on specific requests or training opportunities

**#deal-desk-team** - Deal Desk Team Members 

**#field-ops-team** - Field Ops Team 

**#sales-ops-team** - Sales Ops

**#zuora** - Zuora errors/questions


##### Deal Desk Office Hours

For end of quarter support (Monday, Wednesday, Friday at 12 PM EST ). Calendar invite will be sent to Sales-All Distribuition group. 

Priority will be given to opportunities closing within the quarter. 

Supported topics include:
* Create or modifying a quote
* Quote approval acceleration
* IACV calculation
* Submitting an opportunity for close
* Validation/segmentation of closed opportunities
* And anything else to help drive opportunities closing within the quarter!


## Renewals

Renewal opportunities are automatically generated once a Renewal opportunity or a New Business has been Closed-Won in a previous period. If you don’t have an open Renewal opportunity, please chatter @Sales-support on the Closed-Won opportunity or the Account in SFDC. For all renewal opportunities, the `Opportunity Type` should be `Renewal`.

Please make sure the naming convention of the Renewal opportunity follows [these guidelines](/handbook/business-ops/resources/#opportunity-naming-convention): 

### Calculating Renewal IACV 

To calculate IACV on renewal opportunities, please review the [IACV page of the handbook](/handbook/sales/#incremental-annual-contract-value-iacv). Alternatively, please chatter @Sales-support on the opportunity for assistance in calculating IACV. 

You can also use [this calculator](https://docs.google.com/spreadsheets/d/10hX1ZwTuxa-5PyJr30rTlATClzXmc8i0OunW1u-2D2I/edit#gid=0) to **estimate** the IACV of the renewal.

### Types of Renewals

A renewal can be structured in several different ways. The customer can upgrade, downgrade, coterm, true-up or opt for a flat renewal. 

### Renewal + True up and/or additional seats

Renewal term remains the same as the original subscription, for example we are renewing a 1 year subscription for an additional year. If the term changes, please create a New Subscription. 

1. On the Renewal Opportunity click on “New Quote” Button.
2. Since this is a returning customer, you will need to select the Existing Billing account, if there’s multiple accounts and you are unsure of which one to select, please chatter @sales-support on the opportunity. 
3. Choose “renew existing subscription for this billing account” as quote type and click Next. 
4. Select the Quote Template from the list, Sold to and Bill to Contacts from the Customer. If this is a reseller deal, please include the Reseller Name on the Invoice Owner field and the Reseller Bill to in the Invoice Owner Contact. Also, input the payment terms and any special conditions.
5. The Renewal term must be the same as the original term, i.e 12 months, 24 months. Click on Next. 
6. If they are adding seats of the same product at the same price, please include the total quantity in the existing SKU line. 
7. If the price of the added quantities is different than the price of the quantities renewed, please add a separate line for the new seats. 

    For example:
	Renewal of Starter, quantity: 50, unit price: $39, total price: $1,950.
    New Starter  - 1 Year, quantity: 50, unit price $48, total price: $2,400.

8. If a True-up is required, please add it as New. The Original True up showing on the quote object is only for information purposes and has no impact on the pricing, please do not remove. Their current subscription will reflect the new total number of seats they will be renewing for, which will be equal or greater than the amount they had with their subscription plus the true up amount.
9. Once on the Quote Summary, will click on generate PDF to generate an Order form in PDF.
10. Send it to the customer, via Sertifi button within Zquote screen.
11. Once the quote is signed, please attach it to the opportunity along with the PO and submit the opportunity for approval. The Billing team will review and approve opportunities accorinding to Sales Order Processing guidelines. 

#### Upgrade to a different plan and/or extension of the Subscription Term

We are changing the previous plan purchased, renewal term remains the same as the original subscription, for example we are renewing a 1 year subscription for an additional year. If the term changes, please create a New Subscription. Follow the same steps than in a Standard Renewal but on product selector page remove the old plan and add the new plan with the quantity needed. 

#### Contract resets: 

In the event that the customer wants to reset their contract, this would be considered a renewal. For example, a customer starts January 1st for 12 months, but wants to reset starting April 1st for another 12 month term. Create the quote by cancelling the existing subscription and creating a new opportunity and quote for the new contract term.

### Flat Renewal Support

A flat renewal is when the customer is simply renewing their previous subscription without making any ammendments to users, product type, term, or additional services. The Deal Desk team will assist in creation and processing of Flat Renewals that meet the qualifications outlined below. 

It is the sales teams responsibility to track the renewal and request support for a flat renewal. Flat renewals will not be monitored by Deal Desk unless there is a request for flat renewal support. 

#### Flat Renewal Support Qualifications

To qualify for flat renewal support, the opportunity must meet **all** of the following guidelines: 

*  No customer interaction is required - **no exceptions**
*  
*  Renewal ACV is under $10,000

*  IACV is $0.00 (Flat, no upside or downside/same value year over year)

*  Renewal has no true-up or add-ons and product tier remains the same

*  Renewal will not be co-termed with another existing subscription

*  Customer is in good standing (no billing disputes or collection issues)

*  Billing contact and address is up to date and listed on the opportunity



#### How to request flat renewal support

1. Go to an Opportunity which meets the renewal support criteria
2. Ensure that the billing contact info and billing address are up to date and the contact is listed in the contact section of the opportunity
3. Click on the `Flat Renewal Support` button, located at the top of the opportunity

#### What happens when flat renewal support is requested? 

A case is created within Salesforce and sent to a queue monitored by the Deal Desk team. 

The team will review the opportunity to ensure it meets requirements. If the request does not meet flat renewal support criteria, or if additional information is needed, the Deal Desk team will notify the requestor on the Opportunity chatter. 

The Deal Desk team will create the flat renewal quote that matches the customers previous subscription with the exception of updating the start/end dates. The renewal term will match the previous subscription term unless otherwise stated in the Opportunity chatter by the requestor. 

Once the quote is created, the Deal Desk team will chatter on the opportunity with an attached PDF of the quote. 

Sales Team will be responsible for sending the quote to the customer for review and signature. Once the quote is signed, the sales team will attach it to the opportunity and submit the opportunity for approval, following standard [Sales Order Processing guidelines](/handbook/business-ops/order-processing/). 

The renewal will be reviewed and approved by the Billing Team. 

#### SLA for Flat Renewal Support

SLA for flat renewals is 24 hours, Monday - Friday (Pacific Time). If renewal support is requested after 2:00pm (Pacific Time) on a Friday, the renewal will be processed the following business day.

#### How to Create a Flat Renewal Quote (Deal Desk Team) 
1. On the Renewal Opportunity click on “New Quote” Button.
2. Select the Existing Billing account. If there are multiple billing accounts, select the one that corresponds to the contact/renewing subscription on the opportunity. If it is still unclear, chatter the account manager. 
3. Choose “renew existing subscription for this billing account” as quote type and click Next. 
4. Select the Renewal Quote Template from the list 
5. Update Sold to and Bill to Contacts from the Customer. If this is a reseller deal, please include the Reseller Name on the Invoice Owner field and the Reseller Bill to in the Invoice Owner Contact. Sold To and Bill To should match the customer contact listed on the renewal opportunity. 
6.. Payment terms and contract terms should match the original order. Click Next. 
Enter the subscription term start date.
7.. On the product overview page, no changes should be made, if changes are made it's not a flat renewal. Click submit. 
8.. Once on the Quote Summary, click on “Generate PDF” to generate an Order form in PDF (automatically attaches to opportunity).
9. Send it to the customer contact listed on the opportunity via Sertifi button within Zquote screen.
10. Once Order Form is signed, attach to opportunity and submit for approval. Billing team will review and approve opportunities in accordance to Sales Order Processing guidelines. 

## Assistance with Quotes
If your quote contains any of the following special circumstances, or if you have questions regarding basic quotes, you are encouraged to send chatter the Deal Desk team on the Account or Opportunity record. Please provide as much detail as possible, including links to relevant records, dates, user counts, and other useful information.

#### Contract resets 

If the customer wishes to reset their terms in the middle of their term (for example, they want to upgrade, but want to reset their term for another 12 months), you will need to create a new subscription. In this case, the Subscription Type will be 'New' while the Opportunity Type will be 'Renewal'.

#### Co-term of multiple subscriptions
If the customer has multiple groups and wishes to consolidate their subscriptions, an "Amendment" may be created against a "Renewal Business" opportunity.
Splitting out a single subscription. Conversely, there may be times when a customer needs to split their single subscription into multiple subscriptions. When this occurs, the Subscription Type and Opportunity Type will be 'Renewal'.

#### Ramp Deal/Ramped Pricing
If the prospect or customer would like to employ a ramped pricing schedule where they may want to increase their user count over time. 

Two examples of ramped schedules include:

    Year 1 will be for 100 users and year 2 will be for 200 users.
    Year 1 will be $45 per user per year and year 2 will be $48 per user per year.
    
#### Co-Terming

Co-terming is when a customer wants to align two or more subscriptions with different end dates.

    Example:
    Customer has 2 subscriptions for Premium.
    Subscription 1 = 100 seats, expiring 2018-06-30
    Subscription 2 = 50 seats, expiring 2018-12-31
    
The most likely scenario is to co-term into the subscription ending last:

    On the renewal opportunity for subscription 1, create an amendment quote for subscription 2;
    The start date for the amendment will be 2018-07-01;
    Add 100 seats to the existing 50 and close as usual;

The other scenario is co-terming into the subscription ending first: This requires two steps: first combining the subscriptions, then cancelling the leftover one.

    Create a new opportunity, with a quote amending subscription 1;
    The start date will remain unchanged, e.g. 2018-04-01;
    Add 50 seats to the existing 100 and close as usual;
    Once closed, create a new opportunity to cancel subscription 2 on the day before the start date of the previous quote e.g. 2018-03-31;

## Month End Close Process

The Deal Desk team will assist with Month End Close Process tasks. For a detailed overview of the tasks and responsibilities of Month End, please review [this document](https://docs.google.com/document/d/1Sn-w7a6M91GF9jnt2-5eHuYG3ID6B48qNWZPhI1LKYU). 



